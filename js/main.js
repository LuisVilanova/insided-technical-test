/* Logic for the insided proyect*/
$(function() {

  $("#datepicker").datepicker();

  /* Hihtlight table when checkbox is checked*/
  $('.record_table tr').click(function(event) {
    if (event.target.type !== 'checkbox') {
      $(':checkbox', this).trigger('click');
    }
  });

  $("input[type='checkbox']").change(function(e) {
    if ($(this).is(":checked")) {
      $(this).closest('tr').addClass("highlight_row");
    } else {
      $(this).closest('tr').removeClass("highlight_row");
    }
  });

  /* checkall uncheckall*/

  $('.dropdown-menu').on('click', '#selectAll', function(e) {
    e.stopPropagation(); //prevents dropdown close when click event
    $(this).toggleClass('allChecked');
    if ($(this).hasClass('allChecked')) {
      $('input[type="checkbox"]', '.multi-option').prop('checked', true);
    }
    $(this).toggleClass('allChecked');
  })

  $('.dropdown-menu').on('click', '#unselectAll', function(e) {
    e.stopPropagation();
    $(this).toggleClass('allChecked');
    if ($(this).hasClass('allChecked')) {
      $('input[type="checkbox"]', '.multi-option').prop('checked', false);
    }
    $(this).toggleClass('allChecked');
  })

  $('.dropdown-menu').on('click', 'li', function(e) {
    e.stopPropagation(); //prevents dropdown close inside dropdown multi-option
  })


  /*disabled forms*/

  var $sel1 = $("#select1"),
      $sel2 = $("#select2");
      $sel3 = $("#select3");
      $sel4 = $("#select4");
      $sel5 = $("#select5");
      $datepicker = $("#datepicker");
      $multi_bn = $("#multi-btn");




  $sel1.on("change", function() {
    var _rel = $(this).val();
    if (!_rel) return $sel2.prop("disabled", true);
    $sel2.find("[rel=" + _rel + "]").show();
    $sel2.prop("disabled", false);
  });

  $sel3.on("change", function() {
    var _rel = $(this).val();
    if (!_rel) return $sel4.prop("disabled", true);
    $sel4.find("[rel=" + _rel + "]").show();
    $sel4.prop("disabled", false);
    $datepicker.attr("disabled", false);
  });

  $sel5.on("change", function() {
    var _rel = $(this).val();
    if (!_rel) return $multi_bn.prop("disabled", true);
    $multi_bn.find("[rel=" + _rel + "]").show();
    $multi_bn.attr("disabled", false);

  });

  /*add row - clone*/
  var cloneCount = 1; // Counter for being able to Tag

  $('#add_row').on('click', function() {
    $('.container_1:first')
      .clone(false)
      .attr('id', 'id' + cloneCount++)
      .appendTo('.clone-space');
    $('.container_1 #select2:last').prop("disabled", false);

    $newTag = $("<div class='tag-container'>" + "<p>Tag " + (cloneCount - 1) + "</p> " + "<div class='close-tag'>x</div></div>");
    $('.pagination-container .tag-section').append($newTag);

  });

  /*remove tag and row when click*/

  $(document.body).on('click', '.close-tag', function() {
    $(this).parent().remove();
    $('.container_1:last').remove();
    cloneCount--;


  });


  /*reset selects*/

  $("#btn-reset-1").on("click", function() {
    $sel2.prop("disabled", true);
    $("button#btn-reset-1").css("display", "none");
    $('#select1 option').prop('selected', function() {
      return this.defaultSelected;
    });
    $('#select2 option').prop('selected', function() {
      return this.defaultSelected;
    });


  });

  $("#btn-reset-2").on("click", function() {
    $sel4.prop("disabled", true);
    $datepicker.attr("disabled", true);
    $("button#btn-reset-2").css("display", "none");


    $('#select3 option').prop('selected', function() {
      return this.defaultSelected;
    });
    $('#select4 option').prop('selected', function() {
      return this.defaultSelected;

    });
  });

  $("#btn-reset-3").on("click", function() {
    $("button#btn-reset-3").css("display", "none");
    $('#select5 option').prop('selected', function() {
      return this.defaultSelected;
    });
    $multi_bn.attr("disabled", true);

  });

  /*Reset button functionality*/

  $sel1.on("change", function() {
    if (this.selectedIndex > 0) {
      $("#btn-reset-1").show();
    } else {
      $("#btn-reset-1").hide();
    }
  });

  $sel3.on("change", function() {
    if (this.selectedIndex > 0) {
      $("#btn-reset-2").show();
    } else {
      $("#btn-reset-2").hide();
    }
  });

  $sel5.on("change", function() {
    if (this.selectedIndex > 0) {
      $("#btn-reset-3").show();
    } else {
      $("#btn-reset-3").hide();
    }
  });

  $(".btn-reset").css("diplay", "block");

  /*Select all username in table*/

  var tablechecks = $(".table-user input[type='checkbox']");
      tableDropdown = $('.table-dropdown');

  $('#all-username').change(function() {
    if ($(this).prop('checked')) {
      $('.table-user tr td input[type="checkbox"]').each(function() {
        $(this).prop('checked', true);
      });
    } else {
      $('.table-user tr td input[type="checkbox"]').each(function() {
        $(this).prop('checked', false);
        tableDropdown.css("visibility", "hidden");
      });
    }
  });

  /* if any checkbox is selected, dropdown row apperars*/

  tablechecks.click(function() {
    if (tablechecks.is(":checked")) {
      tableDropdown.css("visibility", "initial");
    } else {
      tableDropdown.css("visibility", "hidden");
    }

  });
});